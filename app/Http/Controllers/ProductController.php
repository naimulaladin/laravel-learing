<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use App\Brand;
use App\Supplier;
use DB;
class ProductController extends Controller
{
    public function index(Request $request)
    {
    	
    	$filter_items = $this->product_filters();
    	
    	return  view('product.index',compact('filter_items'));
    }

    private function product_filters()
    {
    	$data = null;
    	
    	$data['categories'] = Category::get()->pluck('category_name','id')->prepend(' Category','');
    	
    	$data['brands'] = Brand::get()->pluck('brand_name','id')->prepend(' Brand','');
    	
    	$data['suppliers'] = Supplier::get()->pluck('supplier_name','id')->prepend(' Supplier','');
    	
    	return $data;
    }

    public function products_list(Request $request)
    {
    	if ($request->ajax()) {

	    	$products = $this->get_product($request->except(['page']));

    		return  view('product.product_list',compact('products'));
	    	
    	}
    }

    private function insert_query_string($req)
    {
        
    	$query_string = DB::table('query_string')->insert([
    		
    		'string_value'=> http_build_query($req->except(['_token','type'])),

            'type' => $req->type,
    		
    		'client_ip' => $req->ip(),

    	]);
    	return DB::getPdo()->lastInsertId();

    }

    public function product_search(Request $request)
    {   	
        $request->request->add(['type' => 'product']);

    	$query_string_id = $this->insert_query_string($request);
    	
    	$request->session()->flash('ref', 'y');
    	
    	return redirect('search_result/'.$query_string_id);

    }
    public function search_result(Request $request,$id)
    {
    	$query_string = DB::table('query_string')->where('id',$id)->first();
    	
    	parse_str($query_string->string_value,$parameters);
    	
    	$filter_items = $this->product_filters();
    	
    	$request->request->add($parameters);
    	

    	if (!$request->session()->exists('ref')) 
    	{

    		$this->insert_query_string($request);
    	}
            	
    	return  view('product.search_result',compact('filter_items'));   	
    	
    }

    private function column_map($key)
    {
    	$maps = [
    		'category' => 'category_id',
    		'brands' => 'brand_id',
    		'suppliers' => 'supplier_id'
    	];
    	return $maps[$key];
    }


    private function get_product($parameters=[])
    {
    	$products = DB::table('products');
	    	
    	$products->select('products.*','categories.category_name','brands.brand_name','suppliers.supplier_name');
    	
    	$products->leftJoin('categories', 'categories.id','=','products.category_id');
    	
    	$products->leftJoin('brands','brands.id','=','products.brand_id');
    	
    	$products->leftJoin('suppliers', 'suppliers.id','=', 'products.supplier_id');
    	
    	if(count($parameters) > 0){
	    	foreach ($parameters as $key => $value) {
	    		
	    		$products->where($this->column_map($key),$value);
	    	}    		
    	}
    	
    	$products->groupBy('products.id');

    	$products = $products->paginate(5)->appends($parameters);

    	return $products;
    }

    public function reports(Request $request)
    {
        $filter_items = $this->product_filters();
        
        return  view('product.reports',compact('filter_items'));
    }
    
    public function reports_result(Request $request)
    {
        $request->request->add(['type' => 'report']);

        $query_string_id = $this->insert_query_string($request);

        $request->session()->flash('ref', 'y');
        
        return redirect('report_search_result/'.$query_string_id);
    }
    
    public function report_search_result(Request $request,$id)
    {
        $query_string = DB::table('query_string')->where('id',$id)->first();
        
        parse_str($query_string->string_value,$parameters);
        
        $filter_items = $this->product_filters();
        
        $request->request->add($parameters);

        

        return  view('report_search_result',compact('filter_items')); 
    }
    public function report_list(Request $request)
    {
        if ($request->ajax()) {

            $reports = DB::table ('query_string_log');
            
            foreach ($request->except(['page']) as $key => $value) {

                $temp_string = $key.'='.$value;
                
                $reports->whereRaw("find_in_set('$temp_string',q_string)");

            }

            $reports->where('type','product');

            $reports = $reports->paginate(10)->appends($request->all());            

            return  view('report_list_view',compact('reports'));
            
        }
    }
}