<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WebsiteController extends Controller
{
    public function about_us()
    {
    	return view('about_us');
    }
    public function contact_us()
    {
    	return view('contact_us');
    }
}
