<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Student;

class FormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $all_student = [];

        if($request->ajax())
        {
            // print_r($request->all());
            $student = Student::query();
            
            foreach ($request->except(['_token','submit']) as $key => $value) {
                if ($value) 
                {                    
                    $student->where($key,$value);
                }
            }
            return $all_student = $student->get();
            
            
        }
        

        return  view('all_student',compact('all_student'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $student = new Student;
        $student->student_id = $request->student_id;
        $student->student_name = $request->student_name;
        $student->father_name = $request->father_name;
        $student->dob = $request->dob;

        $student->save();

        return redirect('all-student');
    }
   
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
