<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('foo', function () {
    return 'Hello World';
});
Route::get('user/{id}', function ($id) {
    return 'User '.$id;
});
Route::get('user/{name?}', function ($name = null) {
    return $name;
});


Route::view('/welcome', 'welcome', ['name' => 'Taylor']);

Route::get('/about-us', 'WebsiteController@about_us')->name('habijabi');
Route::get('/contact-us', 'WebsiteController@contact_us')->name('hmm');
Route::get('/class_form', function () {
    return view('class_form');
});

Route::post('/form-submit', 'FormController@create');
Route::any('/all-student', 'FormController@index');




Route::get('products_list','ProductController@products_list');

Route::get('products','ProductController@index')->name('products');
Route::post('product_search','ProductController@product_search')->name('product.search');

Route::get('search_result/{id}','ProductController@search_result');

Route::get('reports','ProductController@reports')->name('reports');
Route::post('reports_result','ProductController@reports_result')->name('product.reports');
Route::get('report_search_result/{id}','ProductController@report_search_result')->name('report_search_result');
Route::get('report_list','ProductController@report_list');
