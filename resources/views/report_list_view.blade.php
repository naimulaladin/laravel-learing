<div class="container">
	    Showing {{ $reports->count() }} out of {{ $reports->total() }}
	</div> 

	{{$reports->links()}}

<table class="table table-bordered table-striped">
	<thead>
		<tr>
			<th>Date</th>
			<th>Type</th>
			<th>Client IP</th>
			
		</tr>
	</thead>
	<tbody>
		@foreach($reports as $value)
		<tr>
			<td>{{ $value->log_date }}</td>
			<td>{{ $value->type }}</td>
			<td>{{ $value->client_ip }}</td>
			
		</tr>
		@endforeach
	</tbody>


</table>
<br>