<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
</head>
<body>
	<div class="container">
		<br>
		<form method="get" action="{{URL::to('all-student')}}" class="form-horizontal" id="student_search_form">
			
			<div class="form-row">
				<div class="form-group col">
					<label>student id</label>
					<input type="text" value="{{Request::input('student_id')}}" name="student_id" id="student_id" class="form-control">
				</div>
				<div class="form-group col">
					<label>student name</label>
					<input type="text" value="{{Request::input('student_name')}}" name="student_name" id="student_name" class="form-control">
				</div>
				<div class="form-group col">
					<label>father name</label>
					<input type="text" value="{{Request::input('father_name')}}" name="father_name" id="father_name" class="form-control">
				</div>
				<div class="form-group col">
					<label>dob</label>
					<input type="text" value="{{Request::input('dob')}}" name="dob" id="dob" class="form-control">
				</div>
				<input type="submit" name="submit" value="search" style="display: none;">
				
			</div>
			
		</form>
		<table class="table table-bordered table-striped" id="student-table">
			<tr>
				<th>stdent id</th>
				<th>student name</th>
				<th>father name</th>
				<th>dob</th>
			</tr>
			<tbody id="student-table-body">
				
			</tbody>

			
		</table>
		<!-- value="{{ Request::input('student_id') }}" -->
	</div>
	<script>
		$(document ).ready(function() {
		    $.get("<?php echo Request::fullUrl(); ?>",function(datas){
		    	if (datas) {
		    		load_data(datas);
		    	}
		    },'json');

		    /*$('#student_search_form').submit(function (e) {
                e.preventDefault();
	                var theform = $(this);	                
	                
	                $.ajax({
	                    url: theform.attr('action'),
	                    type: theform.attr('method'),
	                    data: theform.serialize(),
	                    dataType: 'json',
	                    success: function (response) {
	                        if (response) {
	                            load_data(response);
	                        }
	                        else {                           


	                        }
	                    }
	                });
            });*/



		});

		function load_data(datas) {
					$('#student-table-body').html('');
			datas.forEach(function(data) {
					  $('#student-table-body').append(
					  		`<tr> 
					  			<td>${data.student_id}</td> 
					  			<td>${data.student_name}</td> 
					  			<td>${data.father_name}</td>  
					  			<td>${data.dob}</td>  
					  		</tr>`

					  	);
					});
		}
	</script>
</body>
</html>