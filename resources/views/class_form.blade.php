<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
</head>
<body>
	<div class="container">
			<form method="post" id="form1" action="{{url('/form-submit')}}">
				{!! csrf_field() !!}
					<div class="form-row">
						<div class="form-group col">
							Student id: <input type="text" name="student_id" class="form-control">
						</div>
						<div class="form-group col">
							Student Name: <input type="text" name="student_name" class="form-control">
						</div>
						<div class="form-group col">
							Father's Name: <input type="text" name="father_name" class="form-control">
						</div>
						<div class="form-group col">
							Date of Birth<input type="text" name="dob" class="form-control">
						</div>
						<button class="btn btn-primary btn-lg btn-block" type="submit" form="form1" value="Submit">Submit</button>
					</div>
			</form>
	</div>

<!-- <script type="text/javascript">
	
	$(document).ready(function(){
		  
		  $('#form1').submit(function (e) {
                e.preventDefault();
	                var theform = $(this);	                
	                
	                $.ajax({
	                    url: theform.attr('action'),
	                    type: theform.attr('method'),
	                    data: theform.serialize(),
	                    dataType: 'json',
	                    success: function (response) {
	                        if (response) {
	                        	
	                            // load_data(response);
	                        }
	                        else {                           


	                        }
	                    }
	                });
            });

	});
</script> -->



</body>
</html>