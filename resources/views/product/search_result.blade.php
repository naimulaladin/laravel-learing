@extends('master')

@section('content')
	<div class="container">
		<br>
		@include('product.product_search_form',['route'=>'product.search','method'=>'post'])
		
		<div id="product-container">
			loading....
		</div>
	</div>

	<script type="text/javascript">
		$(document ).ready(function() {

			load_product("<?php echo URL::to('products_list').'?'.http_build_query(Request::input()) ?>");

		}); // ready function close

	</script>
	@include('product.product_js')
	
@endsection