<table class="table table-bordered table-striped">
	<thead>
		<tr>
			<th>Product Name</th>
			<th>Category Name</th>
			<th>Brand Name</th>
			<th>Supplier Name</th>
		</tr>
	</thead>
	<tbody>
		@foreach($products as $value)
		<tr>
			<td>{{ $value->product_name }}</td>
			<td>{{ $value->category_name }}</td>
			<td>{{ $value->brand_name }}</td>
			<td>{{ $value->supplier_name }}</td>
		</tr>
		@endforeach
	</tbody>


</table>
<br>

	<div class="container">
	    Showing {{ $products->count() }} out of {{ $products->total() }}
	</div> 

	{{$products->links()}}