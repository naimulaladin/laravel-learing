@extends('master')

@section('content')
	<div class="container">
		<br>
		@include('product.product_search_form',['route'=>'product.reports','method'=>'post'])
		
		<div id="product-container">
			
		</div>
	</div>

	<!-- <script type="text/javascript">
		$(document ).ready(function() {

			load_product("{{ URL::to('products_list') }}");   

		}); // ready function close

		
	</script> -->
	@include('product.product_js')
@endsection