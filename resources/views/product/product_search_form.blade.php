
{!! Form::open(array('route' => $route,'method'=>$method)) !!}
<div class="form-row">
	<div class="form-group col">
		{{ Form::select('category',$filter_items['categories'],Request::input('category'),['class'=>'form-control','id'=>'category']) }}
	</div>
	<div class="form-group col">
		{{ Form::select('brands',$filter_items['brands'],Request::input('brands'),['class'=>'form-control','id'=>'brands']) }}
	</div>
	<div class="form-group col">
		{{ Form::select('suppliers',$filter_items['suppliers'],Request::input('suppliers'),['class'=>'form-control','id'=>'suppliers']) }}
	</div>
	<div class="form-group col">
		{{ Form::submit('Search',['class'=>'btn btn-info']) }}
	</div>			
</div>
<!-- Raw select drop down -->
{{--
<select name="habijabi" class="form-control">
	@php $selected_cat = $_GET['cat']; @endphp

	@foreach($filter_items['categories'] as $key => $value)
	<option value="{{ $key }}" @if($selected_cat == $key) selected="selected" @endif >{{ $value }}</option>
	@endforeach
</select>
<input type="text" name="">
--}}
{!! Form::close() !!}